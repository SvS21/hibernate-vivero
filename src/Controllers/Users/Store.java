package Controllers.Users;

import DAO.DAOUser;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.Base64;
import java.util.ResourceBundle;

public class Store implements Initializable {

    @FXML // fx:id="fullName"
    private Label fullName; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="last_name"
    private TextField last_name; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private TextField username; // Value injected by FXMLLoader

    @FXML // fx:id="phone"
    private TextField phone; // Value injected by FXMLLoader

    @FXML // fx:id="password"
    private PasswordField password; // Value injected by FXMLLoader

    @FXML // fx:id="rolbox"
    private ComboBox<String> rolbox; // Value injected by FXMLLoader

    private static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.fullName.setText(this.getUser().getUsername());
        rolbox.getItems().add("1. Super Admin");
        rolbox.getItems().add("2. Admin");
    }

    @FXML
    void bthHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnSave(MouseEvent event) throws IOException {
        Alert alert;
        if (name.getText().isBlank() || last_name.getText().isBlank() || username.getText().isBlank() || phone.getText().isBlank() || password.getText().isBlank() || rolbox.getSelectionModel().isEmpty()) {
            alert = new Alert(AlertType.ERROR, "Los campos son necesarios", ButtonType.OK);
            alert.showAndWait();
        } else {
            byte[] palabra = password.getText().getBytes();
            String pwdEncrypt = Base64.getEncoder().encodeToString(palabra);
            User user = new User(name.getText(), last_name.getText(), username.getText(), BigInteger.valueOf(Long.parseLong(phone.getText())), pwdEncrypt, Integer.parseInt(rolbox.getSelectionModel().getSelectedItem().substring(0,1)));
            DAOUser daoUser = new DAOUser();
            daoUser.store(user);
            alert = new Alert(AlertType.INFORMATION, "Usuario " + user.getFullName() + " registrado", ButtonType.OK);
            alert.showAndWait();
        }
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Users/Save.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}