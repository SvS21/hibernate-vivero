package Controllers.Users;

import DAO.DAOUser;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Show implements Initializable {

    @FXML // fx:id="usernameLogged"
    private Label usernameLogged; // Value injected by FXMLLoader

    @FXML // fx:id="fullName"
    private Label fullName; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="phone"
    private Label phone; // Value injected by FXMLLoader

    @FXML // fx:id="rol"
    private Label rol; // Value injected by FXMLLoader

    private static User userLogged;
    private static User user;
    private static int id;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.usernameLogged.setText(this.getUserLogged().getUsername());
        DAOUser daoUser = new DAOUser();
        this.setUser(daoUser.show(this.getId()));
        this.fullName.setText(this.getUser().getFullName());
        this.username.setText(this.getUser().getUsername());
        this.phone.setText(String.valueOf(this.getUser().getPhone()));
        if (this.getUser().getRol() == 1)
            this.rol.setText("1. Super admin");
        else
            this.rol.setText("2. Admin");
    }

    @FXML
    void bthHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnEdit(MouseEvent event) throws IOException {
        Update update = new Update();
        update.setUserLogged(this.getUser());
        update.setId(this.getUser().getIdUser());
        update.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Users/Show.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUserLogged() { return userLogged; }

    public void setUserLogged(User userLogged) { this.userLogged = userLogged; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}