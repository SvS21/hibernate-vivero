package Controllers.Users;

import DAO.DAOUser;
import Models.User;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Callback;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.Optional;
import java.util.ResourceBundle;

public class Index implements Initializable {

    @FXML // fx:id="full_name"
    private Label full_name; // Value injected by FXMLLoader

    @FXML // fx:id="table"
    private TableView<User> table; // Value injected by FXMLLoader

    @FXML
    private TableColumn<User, Integer> col_id = new TableColumn<>("ID");

    @FXML
    private TableColumn<User, String> col_full_name = new TableColumn<>("Nombre completo");

    @FXML
    private TableColumn<User, Integer> col_rol = new TableColumn<>("Rol");

    @FXML
    private TableColumn<User, String> col_options = new TableColumn<>("Opciones");

    private static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.full_name.setText(this.getUser().getUsername());
        col_id.setPrefWidth(70);
        col_id.setStyle("-fx-alignment: CENTER");
        col_full_name.setPrefWidth(200);
        col_full_name.setStyle("-fx-alignment: CENTER");
        col_rol.setPrefWidth(170);
        col_rol.setStyle("-fx-alignment: CENTER");
        col_options.setPrefWidth(170);
        col_options.setStyle("-fx-alignment: CENTER");
        table.getColumns().setAll(col_id, col_full_name, col_rol, col_options);
        addButtonShow();
        addButtonEdit();
        addButtonDestroy();
        addButtonChangePassword();
        col_id.setCellValueFactory(new PropertyValueFactory<>("idUser"));
        col_full_name.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        col_rol.setCellValueFactory(new PropertyValueFactory<>("rol"));
        DAOUser daoUser = new DAOUser();
        table.getItems().addAll(daoUser.index());
    }

    private void addButtonShow() {
        TableColumn<User, Void> colBtn = new TableColumn("");
        Callback<TableColumn<User, Void>, TableCell<User, Void>> cellFactory = new Callback<TableColumn<User, Void>, TableCell<User, Void>>() {
            @Override
            public TableCell<User, Void> call(final TableColumn<User, Void> param) {
                final TableCell<User, Void> cell = new TableCell<User, Void>() {
                    private final Button btn = new Button("Ver");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            User user = getTableView().getItems().get(getIndex());
                            Show show = new Show();
                            show.setUserLogged(getUser());
                            show.setId(user.getIdUser());
                            try {
                                show.showView(event);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonEdit() {
        TableColumn<User, Void> colBtn = new TableColumn("");
        Callback<TableColumn<User, Void>, TableCell<User, Void>> cellFactory = new Callback<TableColumn<User, Void>, TableCell<User, Void>>() {
            @Override
            public TableCell<User, Void> call(final TableColumn<User, Void> param) {
                final TableCell<User, Void> cell = new TableCell<User, Void>() {
                    private final Button btn = new Button("Editar");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            User user = getTableView().getItems().get(getIndex());
                            Update update = new Update();
                            update.setId(user.getIdUser());
                            update.setUserLogged(getUser());
                            try {
                                update.showView(event);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonDestroy() {
        TableColumn<User, Void> colBtn = new TableColumn("");
        Callback<TableColumn<User, Void>, TableCell<User, Void>> cellFactory = new Callback<TableColumn<User, Void>, TableCell<User, Void>>() {
            @Override
            public TableCell<User, Void> call(final TableColumn<User, Void> param) {
                final TableCell<User, Void> cell = new TableCell<User, Void>() {
                    private final Button btn = new Button("Eliminar");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            User user = getTableView().getItems().get(getIndex());
                            Alert alert;
                            alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Usuarios");
                            alert.setHeaderText("Estas a punto de hacer una acción irreversible");
                            alert.setContentText("Esta seguro de eliminar el usuario " + user.getName() + "?");
                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.OK) {
                                DAOUser daoUser = new DAOUser();
                                if (daoUser.destroy(user)) {
                                    alert = new Alert(Alert.AlertType.INFORMATION, "Usuario " + user.getFullName() + " eliminado!", ButtonType.OK);
                                    alert.showAndWait();
                                } else {
                                    alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al eliminar el usuario", ButtonType.OK);
                                    alert.showAndWait();
                                }
                                try {
                                    showView(event);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonChangePassword() {
        TableColumn<User, Void> colBtn = new TableColumn("");
        Callback<TableColumn<User, Void>, TableCell<User, Void>> cellFactory = new Callback<TableColumn<User, Void>, TableCell<User, Void>>() {
            @Override
            public TableCell<User, Void> call(final TableColumn<User, Void> param) {
                final TableCell<User, Void> cell = new TableCell<User, Void>() {
                    private final Button btn = new Button("Cambiar contraseña");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            User user = getTableView().getItems().get(getIndex());
                            Alert alert;
                            Dialog<String> dialog = new Dialog<>();
                            dialog.setTitle("Usuarios");
                            dialog.setHeaderText("Cambio de contraseña al usuario " + user.getFullName());
                            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
                            PasswordField pwd = new PasswordField();
                            HBox content = new HBox();
                            content.setAlignment(Pos.CENTER_LEFT);
                            content.setSpacing(10);
                            content.getChildren().addAll(new Label("Ingrese la nueva contraseña"), pwd);
                            dialog.getDialogPane().setContent(content);
                            dialog.setResultConverter(dialogButton -> {
                                if (dialogButton == ButtonType.OK) {
                                    return pwd.getText();
                                }
                                return null;
                            });

                            Optional<String> result = dialog.showAndWait();
                            if (result.isPresent()) {
                                byte[] palabra = result.get().getBytes();
                                String pwdEncrypt = Base64.getEncoder().encodeToString(palabra);
                                user.setPassword(pwdEncrypt);
                                DAOUser daoUser = new DAOUser();
                                if (daoUser.update(user)) {
                                    alert = new Alert(Alert.AlertType.INFORMATION, "Usuario " + user.getFullName() + " ha cambiado su contraseña!", ButtonType.OK);
                                    alert.showAndWait();
                                } else {
                                    alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al actualizar la contraseña del usuario", ButtonType.OK);
                                    alert.showAndWait();
                                }
                                try {
                                    showView(event);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    @FXML
    void btnAddUser(MouseEvent event) throws IOException {
        Store store = new Store();
        store.setUser(this.getUser());
        store.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Users/Index.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}