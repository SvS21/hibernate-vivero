package Controllers.Users;

import DAO.DAOUser;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.ResourceBundle;

public class Update implements Initializable {

    @FXML // fx:id="title"
    private Label title; // Value injected by FXMLLoader

    @FXML // fx:id="fullName"
    private Label fullName; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="last_name"
    private TextField last_name; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private TextField username; // Value injected by FXMLLoader

    @FXML // fx:id="phone"
    private TextField phone; // Value injected by FXMLLoader

    @FXML // fx:id="rolbox"
    private ComboBox<String> rolbox; // Value injected by FXMLLoader

    private static User userLogged;
    private static User user;
    private static int id;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.fullName.setText(this.getUserLogged().getUsername());
        rolbox.getItems().add("1. Super Admin");
        rolbox.getItems().add("2. Admin");
        DAOUser daoUser = new DAOUser();
        this.setUser(daoUser.show(this.getId()));
        this.title.setText("Editando al usuario : " + this.getUser().getFullName());
        name.setText(this.getUser().getName());
        last_name.setText(this.getUser().getLast_name());
        username.setText(this.getUser().getUsername());
        phone.setText(String.valueOf(this.getUser().getPhone()));
        String rol;
        if (this.getUser().getRol() == 1)
            rol = "1. Super Admin";
        else
            rol = "2. Admin";
        rolbox.getSelectionModel().select(rol);
    }

    @FXML
    void bthHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUserLogged());
        dashboard.showView(event);
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUserLogged());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUserLogged());
        index.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUserLogged());
        index.showView(event);
    }

    @FXML
    void btnSave(MouseEvent event) throws IOException {
        Alert alert;
        if (name.getText().isBlank() || last_name.getText().isBlank() || username.getText().isBlank() || phone.getText().isBlank() || rolbox.getSelectionModel().isEmpty()) {
            alert = new Alert(Alert.AlertType.ERROR, "Los campos son necesarios", ButtonType.OK);
            alert.showAndWait();
        } else {
            this.getUser().setName(name.getText());
            this.getUser().setLast_name(last_name.getText());
            this.getUser().setUsername(username.getText());
            this.getUser().setPhone(BigInteger.valueOf(Long.parseLong(phone.getText())));
            String rol = rolbox.getSelectionModel().getSelectedItem().substring(0,1);
            this.getUser().setRol(Integer.parseInt(rol));
            DAOUser daoUser = new DAOUser();
            if (daoUser.update(this.getUser())) {
                alert = new Alert(Alert.AlertType.INFORMATION, "Usuario " + this.getUser().getUsername() + " actualizado!", ButtonType.OK);
                alert.showAndWait();
            } else {
                alert = new Alert(Alert.AlertType.ERROR, "Error al actualizar el usuario " + this.getUser().getUsername(), ButtonType.OK);
                alert.showAndWait();
            }
            Index index = new Index();
            index.setUser(this.getUserLogged());
            index.showView(event);
        }
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUserLogged());
        index.showView(event);
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Users/Edit.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUserLogged() { return userLogged; }

    public void setUserLogged(User userLogged) { this.userLogged = userLogged; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}