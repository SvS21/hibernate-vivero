package Controllers.Products;

import DAO.DAOCategory;
import DAO.DAOProduct;
import Models.Category;
import Models.Product;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Store implements Initializable {

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="category"
    private ComboBox<Category> category; // Value injected by FXMLLoader

    @FXML // fx:id="condition"
    private ComboBox<String> condition; // Value injected by FXMLLoader

    public static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.username.setText(this.getUser().getUsername());
        DAOCategory daoCategory = new DAOCategory();
        category.getItems().setAll(daoCategory.index());
        condition.getItems().addAll("1. Buen estado", "2. Falta de agua", "3. Exceso de agua", "4. Con plaga");
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnSave(MouseEvent event) throws IOException {
        Alert alert;
        if (name.getText().isBlank() || category.getSelectionModel().isEmpty() || condition.getSelectionModel().isEmpty()) {
            alert = new Alert(Alert.AlertType.ERROR, "Los campos son requeridos", ButtonType.OK);
            alert.showAndWait();
        } else {
            Product product = new Product(name.getText(), category.getSelectionModel().getSelectedItem(), Integer.parseInt(condition.getSelectionModel().getSelectedItem().substring(0,1)));
            DAOProduct daoProduct = new DAOProduct();
            if (daoProduct.store(product)) {
                alert = new Alert(Alert.AlertType.INFORMATION, "Producto " + product.getName() + " registrado con exito!", ButtonType.OK);
                alert.showAndWait();
            } else {
                alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al registrar el producto " + product.getName(), ButtonType.OK);
                alert.showAndWait();
            }
            Index index = new Index();
            index.setUser(this.getUser());
            index.showView(event);
        }
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Products/Save.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}