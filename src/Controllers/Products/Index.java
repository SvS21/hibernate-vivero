package Controllers.Products;
;
import DAO.DAOProduct;
import Models.Product;
import Models.User;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class Index implements Initializable {

    @FXML // fx:id="fullname"
    private Label fullname; // Value injected by FXMLLoader

    @FXML // fx:id="table"
    private TableView<Product> table; // Value injected by FXMLLoader

    @FXML
    private TableColumn<Product, Integer> col_id = new TableColumn<>("ID");

    @FXML
    private TableColumn<Product, String> col_name = new TableColumn<>("Nombre");

    @FXML
    private TableColumn<Product, String> col_options = new TableColumn<>("Opciones");

    private static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        col_id.setPrefWidth(70);
        col_id.setStyle("-fx-alignment: CENTER");
        col_name.setPrefWidth(150);
        col_name.setStyle("-fx-alignment: CENTER");
        col_options.setPrefWidth(100);
        col_options.setStyle("-fx-alignment: CENTER");
        table.getColumns().addAll(col_id, col_name, col_options);
        this.fullname.setText(this.getUser().getUsername());
        addButtonShow();
        addButtonEdit();
        addButtonDestroy();
        col_id.setCellValueFactory(new PropertyValueFactory<>("idProduct"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        DAOProduct daoProduct = new DAOProduct();
        table.setItems(FXCollections.observableList(daoProduct.index()));
    }

    private void addButtonShow() {
        TableColumn<Product, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Product, Void>, TableCell<Product, Void>> cellFactory = new Callback<TableColumn<Product, Void>, TableCell<Product, Void>>() {
            @Override
            public TableCell<Product, Void> call(final TableColumn<Product, Void> param) {
                final TableCell<Product, Void> cell = new TableCell<Product, Void>() {
                    private final Button btn = new Button("Ver");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Product product = getTableView().getItems().get(getIndex());
                            Show show = new Show();
                            show.setId(product.getIdProduct());
                            show.setUser(getUser());
                            try {
                                show.showView(event);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonEdit() {
        TableColumn<Product, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Product, Void>, TableCell<Product, Void>> cellFactory = new Callback<TableColumn<Product, Void>, TableCell<Product, Void>>() {
            @Override
            public TableCell<Product, Void> call(final TableColumn<Product, Void> param) {
                final TableCell<Product, Void> cell = new TableCell<Product, Void>() {
                    private final Button btn = new Button("Editar");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Product product = getTableView().getItems().get(getIndex());
                            Update update = new Update();
                            update.setId(product.getIdProduct());
                            update.setUser(getUser());
                            try {
                                update.showView(event);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonDestroy() {
        TableColumn<Product, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Product, Void>, TableCell<Product, Void>> cellFactory = new Callback<TableColumn<Product, Void>, TableCell<Product, Void>>() {
            @Override
            public TableCell<Product, Void> call(final TableColumn<Product, Void> param) {
                final TableCell<Product, Void> cell = new TableCell<Product, Void>() {
                    private final Button btn = new Button("Eliminar");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Product product = getTableView().getItems().get(getIndex());
                            Alert alert;
                            alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Productos");
                            alert.setHeaderText("Estas a punto de hacer una acción irreversible");
                            alert.setContentText("Esta seguro de eliminar el producto " + product.getName() + "?");
                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.OK) {
                                DAOProduct daoProduct = new DAOProduct();
                                if (daoProduct.destroy(product)) {
                                    alert = new Alert(Alert.AlertType.INFORMATION, "Producto: " + product.getName() + " Eliminado", ButtonType.OK);
                                    alert.showAndWait();
                                }
                                try {
                                    showView(event);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    @FXML
    void btnAddProduct(MouseEvent event) throws IOException {
        Store store = new Store();
        store.setUser(this.getUser());
        store.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Products/Index.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}