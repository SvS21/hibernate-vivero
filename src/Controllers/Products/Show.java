package Controllers.Products;

import Controllers.Categories.Index;
import DAO.DAOProduct;
import Models.Product;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class Show implements Initializable {

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private Label name; // Value injected by FXMLLoader

    @FXML // fx:id="image"
    private ImageView image; // Value injected by FXMLLoader

    @FXML // fx:id="category"
    private Label category; // Value injected by FXMLLoader

    @FXML // fx:id="current_date"
    private Label current_date; // Value injected by FXMLLoader

    @FXML // fx:id="next_water"
    private Label next_water; // Value injected by FXMLLoader

    @FXML // fx:id="condition"
    private Label condition; // Value injected by FXMLLoader

    @FXML // fx:id="created"
    private Label created; // Value injected by FXMLLoader

    private static Product product;
    private static User user;
    private static int id;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setText(this.getUser().getUsername());
        DAOProduct daoProduct = new DAOProduct();
        this.setProduct(daoProduct.show(this.getId()));
        name.setText(this.getProduct().getName());
        category.setText(this.getProduct().getCategory().getName());
        try {
            SimpleDateFormat parseador = new SimpleDateFormat("dd-MM-yy");
            SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yy");
            Date date = parseador.parse(this.getProduct().getCreated_at());
            created.setText(formateador.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (this.getProduct().getCondition() == 1)
            condition.setText("1. Buen estado");
        else if (this.getProduct().getCondition() == 2)
            condition.setText("2. Falta de agua");
        else if (this.getProduct().getCondition() == 3)
            condition.setText("3. Exceso de agua");
        else if (this.getProduct().getCondition() == 4)
            condition.setText("4. Con plaga");
    }

    @FXML
    void btnBack(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnEdit(MouseEvent event) throws IOException {
        Update update = new Update();
        update.setUser(this.getUser());
        update.setId(this.getId());
        update.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnImages(MouseEvent event) {    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        Controllers.Users.Index index = new Controllers.Users.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnWaters(MouseEvent event) {    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Products/Show.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { Show.product = product; }

    public User getUser() { return user; }

    public void setUser(User user) { Show.user = user; }

    public int getId() { return id; }

    public void setId(int id) { Show.id = id; }
}