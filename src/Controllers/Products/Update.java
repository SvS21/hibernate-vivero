package Controllers.Products;

import DAO.DAOCategory;
import DAO.DAOProduct;
import Models.Category;
import Models.Product;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Update implements Initializable {

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="category"
    private ComboBox<Category> category; // Value injected by FXMLLoader

    @FXML // fx:id="condition"
    private ComboBox<String> condition; // Value injected by FXMLLoader

    private static Product product;
    private static User user;
    private static int id;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.username.setText(this.getUser().getUsername());
        DAOCategory daoCategory = new DAOCategory();
        category.getItems().setAll(daoCategory.index());
        condition.getItems().addAll("1. Buen estado", "2. Falta de agua", "3. Exceso de agua", "4. Con plaga");
        DAOProduct daoProduct = new DAOProduct();
        this.setProduct(daoProduct.show(this.getId()));
        name.setText(this.getProduct().getName());
        category.getSelectionModel().select(this.getProduct().getCategory());
        String aux = null;
        if (this.getProduct().getCondition() == 1)
            aux = "1.- Buen estado";
        if (this.getProduct().getCondition() == 2)
            aux = "2.- Falta de agua";
        if (this.getProduct().getCondition() == 3)
            aux = "3.- Exceso de agua";
        if (this.getProduct().getCondition() == 4)
            aux = "4.- Con plaga";
        condition.getSelectionModel().select(aux);
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Controllers.Categories.Index index = new Controllers.Categories.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogout(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnSave(MouseEvent event) throws IOException {
        Alert alert;
        if (name.getText().isBlank() || category.getSelectionModel().getSelectedItem() == null || condition.getSelectionModel().getSelectedItem() == null) {
            alert = new Alert(Alert.AlertType.ERROR, "Los campos son requeridos", ButtonType.OK);
            alert.showAndWait();
        } else {
            this.getProduct().setName(name.getText());
            this.getProduct().setCategory(category.getSelectionModel().getSelectedItem());
            this.getProduct().setCondition(Integer.valueOf(condition.getSelectionModel().getSelectedItem().substring(0,1)));
            DAOProduct daoProduct = new DAOProduct();
            if (daoProduct.update(this.getProduct()))
                alert = new Alert(Alert.AlertType.INFORMATION, "Producto " + product.getName() + " actualizado con exito!", ButtonType.OK);
            else
                alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al actualizar el producto " + product.getName(), ButtonType.OK);
            alert.showAndWait();
            Index index = new Index();
            index.setUser(this.getUser());
            index.showView(event);
        }
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Products/Edit.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }
}