package Controllers.Categories;

import DAO.DAOCategory;
import Models.Category;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Update implements Initializable {

    @FXML // fx:id="title"
    private Label title; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="father"
    private ComboBox<Category> father; // Value injected by FXMLLoader

    private static Category category;
    private static User user;
    private static int id;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setText(this.getUser().getUsername());
        DAOCategory daoCategory = new DAOCategory();
        this.setCategory(daoCategory.show(this.getId()));
        title.setText("Editando categoría : " + this.getCategory().getName());
        name.setText(this.getCategory().getName());
        if (this.getCategory().getCategory_id() == null) {
            father.getItems().add(null);
            father.getSelectionModel().select(null);
        } else
            father.getSelectionModel().select(daoCategory.show(this.getCategory().getCategory_id()));
        father.getItems().setAll(daoCategory.index());
        System.out.println("Category_id : " + this.getCategory().getCategory_id());
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Index index = new Index();
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.showView(event);
    }

    @FXML
    void btnLogOut(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnSave(MouseEvent event) throws IOException {
        Alert alert;
        if (name.getText().isBlank()) {
            alert = new Alert(Alert.AlertType.ERROR, "El nombre es requerido", ButtonType.OK);
            alert.showAndWait();
        } else  {
            if (father.getSelectionModel().isEmpty() || father.getSelectionModel().getSelectedItem() == null)
                this.getCategory().setCategory_id(null);
            else {
                String cat = father.getSelectionModel().getSelectedItem().toString();
                String aux = cat.substring(cat.indexOf("=")+1, cat.indexOf(","));
                this.getCategory().setCategory_id(Integer.parseInt(aux));
            }
            this.getCategory().setName(name.getText());
            DAOCategory daoCategory = new DAOCategory();
            if (daoCategory.update(this.getCategory()))
                alert = new Alert(Alert.AlertType.INFORMATION, this.getCategory().getName()+" actualizada!", ButtonType.OK);
            else
                alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al actualizar", ButtonType.OK);
            alert.showAndWait();
        }
        Index index = new Index();
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Categories/Edit.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public Category getCategory() { return category; }

    public void setCategory(Category category) { this.category = category; }
}