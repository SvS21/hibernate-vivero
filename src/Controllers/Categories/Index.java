package Controllers.Categories;

import DAO.DAOCategory;
import Models.Category;
import Models.User;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class Index implements Initializable {

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="table"
    private TableView<Category> table; // Value injected by FXMLLoader

    @FXML
    private TableColumn<Category, Integer> col_id = new TableColumn<>("ID");

    @FXML
    private TableColumn<Category, String> col_name = new TableColumn<>("Nombre");

    @FXML
    private TableColumn<Category, String> col_options = new TableColumn<>("Opciones");

    private static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        col_id.setPrefWidth(70);
        col_id.setStyle("-fx-alignment: CENTER;");
        col_name.setPrefWidth(150);
        col_name.setStyle("-fx-alignment: CENTER;");
        col_options.setPrefWidth(100);
        col_options.setStyle("-fx-alignment: CENTER;");
        table.getColumns().addAll(col_id, col_name, col_options);
        this.username.setText(this.getUser().getUsername());
        addButtonShow();
        addButtonEdit();
        addButtonDestroy();
        col_id.setCellValueFactory(new PropertyValueFactory<>("idCategory"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        DAOCategory daoCategory = new DAOCategory();
        table.setItems(FXCollections.observableList(daoCategory.index()));
    }

    private void addButtonShow() {
        TableColumn<Category, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Category, Void>, TableCell<Category, Void>> cellFactory = new Callback<TableColumn<Category, Void>, TableCell<Category, Void>>() {
            @Override
            public TableCell<Category, Void> call(final TableColumn<Category, Void> param) {
                final TableCell<Category, Void> cell = new TableCell<Category, Void>() {
                    private final Button btn = new Button("Ver");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Category category = getTableView().getItems().get(getIndex());
                            Show show = new Show();
                            show.setUser(getUser());
                            show.setId(category.getIdCategory());
                            try {
                                show.showView(event);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonEdit() {
        TableColumn<Category, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Category, Void>, TableCell<Category, Void>> cellFactory = new Callback<TableColumn<Category, Void>, TableCell<Category, Void>>() {
            @Override
            public TableCell<Category, Void> call(final TableColumn<Category, Void> param) {
                final TableCell<Category, Void> cell = new TableCell<Category, Void>() {
                    private final Button btn = new Button("Editar");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Category category = getTableView().getItems().get(getIndex());
                            Update update = new Update();
                            update.setUser(getUser());
                            update.setId(category.getIdCategory());
                            try {
                                update.showView(event);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    private void addButtonDestroy() {
        TableColumn<Category, Void> colBtn = new TableColumn("");
        Callback<TableColumn<Category, Void>, TableCell<Category, Void>> cellFactory = new Callback<TableColumn<Category, Void>, TableCell<Category, Void>>() {
            @Override
            public TableCell<Category, Void> call(final TableColumn<Category, Void> param) {
                final TableCell<Category, Void> cell = new TableCell<Category, Void>() {
                    private final Button btn = new Button("Eliminar");
                    {
                        btn.setOnAction((ActionEvent event) -> {
                            Category category = getTableView().getItems().get(getIndex());
                            Alert alert;
                            alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Categorías");
                            alert.setHeaderText("Estas a punto de hacer una acción irreversible");
                            alert.setContentText("Esta seguro de eliminar la categoría " + category.getName() + "?");
                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.OK) {
                                DAOCategory daoCategory = new DAOCategory();
                                if (daoCategory.destroy(category)) {
                                    alert = new Alert(Alert.AlertType.INFORMATION, "Categoría: " + category.getName() + " Eliminada", ButtonType.OK);
                                    alert.showAndWait();
                                }
                                try {
                                    showView(event);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };
        colBtn.setCellFactory(cellFactory);
        col_options.getColumns().add(colBtn);
    }

    @FXML
    void btnAddCategory(MouseEvent event) throws IOException {
        Store store = new Store();
        store.setUser(this.getUser());
        store.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogOut(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Categories/Index.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}