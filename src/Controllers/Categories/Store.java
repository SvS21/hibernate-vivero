package Controllers.Categories;

import DAO.DAOCategory;
import Models.Category;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Store implements Initializable {

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="name"
    private TextField name; // Value injected by FXMLLoader

    @FXML // fx:id="father"
    private ComboBox<Category> father; // Value injected by FXMLLoader

    private static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.username.setText(this.getUser().getUsername());
        DAOCategory daoCategory = new DAOCategory();
        father.getItems().setAll(daoCategory.index());
    }

    @FXML
    void btnCancel(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogOut(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnSave(MouseEvent event) throws IOException {
        Alert alert;
        Category category;
        if (name.getText().isBlank()) {
            alert = new Alert(Alert.AlertType.ERROR, "El nombre es requerido", ButtonType.OK);
            alert.showAndWait();
        } else {
            if (father.getSelectionModel().isEmpty())
                category = new Category(name.getText());
            else {
                String cat = father.getSelectionModel().getSelectedItem().toString();
                String aux = cat.substring(cat.indexOf("=")+1, cat.indexOf(","));
                category = new Category(name.getText(), Integer.parseInt(aux));
            }
            DAOCategory daoCategory = new DAOCategory();
            if (daoCategory.store(category))
                alert = new Alert(Alert.AlertType.INFORMATION, "Categoría registrada con exito!", ButtonType.OK);
            else
                alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al registrar la categoría", ButtonType.OK);
            alert.showAndWait();
        }
        showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Categories/Save.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }
}