package Controllers.Categories;

import DAO.DAOCategory;
import Models.Category;
import Models.Product;
import Models.User;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.DashboardController;
import sample.LoginController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Show implements Initializable {

    @FXML // fx:id="title"
    private Label title; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="table"
    private TableView<Product> table; // Value injected by FXMLLoader

    @FXML
    private TableColumn<Product, Integer> col_id = new TableColumn<>("ID");

    @FXML
    private TableColumn<Product, String> col_name = new TableColumn<>("Nombre Producto");

    @FXML // fx:id="category_id"
    private Label category_id; // Value injected by FXMLLoader

    private static User user;
    private static int id;
    private static Category category;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        username.setText(this.getUser().getUsername());
        DAOCategory daoCategory = new DAOCategory();
        this.setCategory(daoCategory.show(this.getId()));
        title.setText("Categoría : " + this.getCategory().getName());
        if (this.getCategory().getCategory_id() != null)
            category_id.setText("Categoría padre: " + daoCategory.show(this.getCategory().getCategory_id()).toString());
        else
            category_id.setText("Categoría padre: Ninguna");
        col_id.setPrefWidth(100);
        col_id.setStyle("-fx-alignment: CENTER");
        col_name.setPrefWidth(200);
        col_name.setStyle("-fx-alignment: CENTER");
        table.getColumns().addAll(col_id, col_name);
        col_id.setCellValueFactory(new PropertyValueFactory<>("idProduct"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        table.setItems(FXCollections.observableArrayList(this.getCategory().getProducts()));
    }

    @FXML
    void btnBack(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnHome(MouseEvent event) throws IOException {
        DashboardController dashboard = new DashboardController();
        dashboard.setUser(this.getUser());
        dashboard.showView(event);
    }

    @FXML
    void btnLogOut(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        Controllers.Users.Index index = new Controllers.Users.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Categories/Show.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public Category getCategory() { return category; }

    public void setCategory(Category category) { Show.category = category; }

    public User getUser() { return user; }

    public void setUser(User user) { Show.user = user; }

    public int getId() { return id; }

    public void setId(int id) { Show.id = id; }
}