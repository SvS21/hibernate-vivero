package sample;

import DAO.DAOUser;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import java.io.IOException;

public class LoginController {

    @FXML // fx:id="input_email"
    private TextField input_email; // Value injected by FXMLLoader

    @FXML // fx:id="input_password"
    private PasswordField input_password; // Value injected by FXMLLoader

    @FXML
    void btnOnMouseClickedLogin(MouseEvent event) throws IOException {
        DAOUser daoUser = new DAOUser();
        User user = daoUser.login(input_email.getText(), input_password.getText());
        if (user.getUsername() != null) {
            DashboardController dashboard = new DashboardController();
            dashboard.setUser(user);
            dashboard.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Credenciales no validas", ButtonType.OK);
            alert.showAndWait();
            System.exit(1);
        }
    }

    @FXML
    void btnOnMouseClickedRegister(MouseEvent event) throws IOException {
        RegisterController register = new RegisterController();
        register.showView(event);
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/login.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }
}