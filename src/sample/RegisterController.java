package sample;

import DAO.DAOUser;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Base64;

public class RegisterController {

    @FXML // fx:id="input_name"
    private TextField input_name; // Value injected by FXMLLoader

    @FXML // fx:id="input_last_name"
    private TextField input_last_name; // Value injected by FXMLLoader

    @FXML // fx:id="input_username"
    private TextField input_username; // Value injected by FXMLLoader

    @FXML // fx:id="input_phone"
    private TextField input_phone; // Value injected by FXMLLoader

    @FXML // fx:id="input_password"
    private PasswordField input_password; // Value injected by FXMLLoader

    @FXML
    void btnOnMouseClickedLogin(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnOnMouseClickedRegister(MouseEvent event) throws IOException {
        Alert alert;
        if (input_name.getText().isBlank() || input_last_name.getText().isBlank() || input_username.getText().isBlank() || input_password.getText().isBlank() || input_phone.getText().isBlank()) {
            alert = new Alert(Alert.AlertType.ERROR, "Los campos son obligatorios", ButtonType.OK);
            alert.showAndWait();
        } else {
            byte[] palabra = input_password.getText().getBytes();
            String pwdEncrypt = Base64.getEncoder().encodeToString(palabra);
            User user = new User(input_name.getText(), input_last_name.getText(), input_username.getText(), BigInteger.valueOf(Long.parseLong(input_phone.getText())), pwdEncrypt, 2);
            DAOUser daoUser = new DAOUser();
            if (daoUser.store(user)) {
                alert = new Alert(Alert.AlertType.INFORMATION, "Usuario: "+user.getUsername()+" registrado con exito!", ButtonType.OK);
                alert.showAndWait();
                DashboardController dashboard = new DashboardController();
                dashboard.setUser(user);
                dashboard.showView(event);
            } else alert = new Alert(Alert.AlertType.ERROR, "Ocurrio un error al registrar al usuario: "+user.getUsername(), ButtonType.OK); alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/register.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }
}