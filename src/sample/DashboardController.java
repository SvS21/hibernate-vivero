package sample;

import Controllers.Categories.Index;
import DAO.DAOCategory;
import DAO.DAOProduct;
import DAO.DAOUser;
import Models.User;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class DashboardController implements Initializable {

    @FXML // fx:id="welcomeUser"
    private Label welcomeUser; // Value injected by FXMLLoader

    @FXML // fx:id="usernameCard"
    private Label usernameCard; // Value injected by FXMLLoader

    @FXML // fx:id="userRol"
    private Label userRol; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="productsCount"
    private Label productsCount; // Value injected by FXMLLoader

    @FXML // fx:id="categoriesCount"
    private Label categoriesCount; // Value injected by FXMLLoader

    @FXML // fx:id="userCount"
    private Label userCount; // Value injected by FXMLLoader

    private static User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.username.setText(this.getUser().getFullName());
        this.usernameCard.setText(getUser().getUsername());
        this.welcomeUser.setText("Bienvenido, " + getUser().getUsername());
        String userRol = (getUser().getRol() == 1) ? ("Super Adm") : ("Adm General");
        this.userRol.setText(userRol);
        DAOUser daoUser = new DAOUser();
        DAOCategory daoCategory = new DAOCategory();
        DAOProduct daoProduct = new DAOProduct();
        this.userCount.setText(String.valueOf(daoUser.count()));
        this.categoriesCount.setText(String.valueOf(daoCategory.count()));
        this.productsCount.setText(String.valueOf(daoProduct.count()));
    }

    @FXML
    void btnCategories(MouseEvent event) throws IOException {
        Index index = new Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnLogOut(MouseEvent event) throws IOException {
        LoginController login = new LoginController();
        login.showView(event);
    }

    @FXML
    void btnProducts(MouseEvent event) throws IOException {
        Controllers.Products.Index index = new Controllers.Products.Index();
        index.setUser(this.getUser());
        index.showView(event);
    }

    @FXML
    void btnUsers(MouseEvent event) throws IOException {
        if (this.getUser().getRol() == 1) {
            Controllers.Users.Index index = new Controllers.Users.Index();
            index.setUser(this.getUser());
            index.showView(event);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No cuentas con los permisos suficientes", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void showView(Event event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/views/dashboard.fxml"));
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.toFront();
        stage.show();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}