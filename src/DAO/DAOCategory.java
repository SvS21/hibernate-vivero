package DAO;

import Models.Category;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import java.util.List;

public class DAOCategory {

    private static SessionFactory factory;
    private static ServiceRegistry serviceRegistry;

    public DAOCategory() {
        System.out.println("Starting...");
        try {
            Configuration configuration = new Configuration();
            configuration.configure("/configs/hibernate-mysql.cfg.xml");
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            factory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Error: "+ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * method to save a new category
     * @param /Models.Category category
     * @return Boolean True
     */
    public boolean store(Category category) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(category);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to get all Categories
     * @return List<Category>
     */
    public List<Category> index() {
        Session session = factory.openSession();
        Transaction tx = null;
        List categories = null;
        try {
            tx = session.beginTransaction();
            categories = session.createCriteria(Category.class).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return categories;
    }

    /**
     * method to get a Category
     * @param /Integer id
     * @return /Models.Category category
     */
    public Category show(Integer id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Category category = null;
        try {
            tx = session.beginTransaction();
            category = (Category) session.createCriteria(Category.class)
                    .setFetchMode("products", FetchMode.JOIN)
                    .add(Restrictions.idEq(id)).uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return category;
    }

    /**
     * method to update a /Models.Category category
     * @return Boolean true
     */
    public boolean update(Category category) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(category);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to delete a category
     * @param /Models.Category category
     * @return Boolean true
     */
    public boolean destroy(Category category) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(category);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to get the number of registered categories.
     * @return Integer countCategories
     */
    public long count() {
        Session session = factory.openSession();
        Transaction tx = null;
        long countCategories = 0;
        try {
            tx = session.beginTransaction();
            countCategories = (long) session.createCriteria(Category.class)
                    .setProjection(Projections.rowCount()).uniqueResult();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx!=null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return countCategories;
    }
}