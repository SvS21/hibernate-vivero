package DAO;

import Models.Water;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.List;

public class DAOWater {

    private static SessionFactory factory;
    private static ServiceRegistry serviceRegistry;

    public DAOWater() {
        System.out.println("Starting...");
        try {
            Configuration configuration = new Configuration();
            configuration.configure("/configs/hibernate-mysql.cfg.xml");
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            factory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Error: "+ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * method to save a new water
     * @param /Models.Water water
     */
    public void store(Water water) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(water);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * method to get all Waters
     * @return List<Water>
     */
    public List<Water> index() {
        Session session = factory.openSession();
        Transaction tx = null;
        List waters = null;
        try {
            tx = session.beginTransaction();
            waters = session.createCriteria(Water.class).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return waters;
    }
}