package DAO;

import Models.User;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Base64;
import java.util.List;

public class DAOUser {

    private static SessionFactory factory;
    private static ServiceRegistry serviceRegistry;

    public DAOUser() {
        System.out.println("Starting...");
        try {
            Configuration configuration = new Configuration();
            configuration.configure("/configs/hibernate-mysql.cfg.xml");
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            factory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Error: " + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * method to save a new user
     *
     * @param /Models.User user
     * @return Boolean true
     */
    public boolean store(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to get all users
     * @return List<User>
     */
    public List<User> index() {
        Session session = factory.openSession();
        Transaction tx = null;
        List users = null;
        try {
            tx = session.beginTransaction();
            users = session.createCriteria(User.class).list();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return users;
    }

    /**
     * method to get a User
     * @param /Integer id
     * @return /Models.User user
     */
    public User show(Integer id) {
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            user = (User) session.createCriteria(User.class)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    /**
     * method to update a User
     * @param /Models.User user
     * @return Boolean true
     */
    public Boolean update(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to delete a user
     * @param /Models.User user
     * @return Boolean true
     */
    public Boolean destroy(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(user);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to get the number of registered users.
     * @return Integer countUsers
     */
    public long count() {
        Session session = factory.openSession();
        Transaction tx = null;
        long countUsers = 0;
        try {
            tx = session.beginTransaction();
            countUsers = (long) session.createCriteria(User.class)
                    .setProjection(Projections.rowCount()).uniqueResult();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx!=null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return countUsers;
    }

    /**
     * method to check a user with the username = ? and password
     * @return /Models.User user
     */
    public User login(String username, String pwd) {
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            byte[] palabra = pwd.getBytes();
            String pwdEncrypt = Base64.getEncoder().encodeToString(palabra);
            user = (User) session.createCriteria(User.class)
                    .add(Restrictions.eq("username", username))
                    .add(Restrictions.eq("password", pwdEncrypt)).uniqueResult();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
}