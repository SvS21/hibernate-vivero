package DAO;

import Models.Image;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.List;

public class DAOImage {

    private static SessionFactory factory;
    private static ServiceRegistry serviceRegistry;

    public DAOImage() {
        System.out.println("Starting...");
        try {
            Configuration configuration = new Configuration();
            configuration.configure("/configs/hibernate-mysql.cfg.xml");
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            factory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Error: "+ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * method to save a new image
     * @param /Models.Image image
     */
    public void store(Image image) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(image);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * method to get all Images
     * @return ObservableList<Image>
     */
    public ObservableList<Image> index() {
        Session session = factory.openSession();
        Transaction tx = null;
        List auxImages = null;
        ObservableList images = null;
        try {
            tx = session.beginTransaction();
            auxImages = session.createCriteria(Image.class).list();
            images = FXCollections.observableArrayList(auxImages);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return images;
    }
}