package DAO;

import Models.Product;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import java.util.List;

public class DAOProduct {

    private static SessionFactory factory;
    private static ServiceRegistry serviceRegistry;

    public DAOProduct() {
        System.out.println("Starting...");
        try {
            Configuration configuration = new Configuration();
            configuration.configure("/configs/hibernate-mysql.cfg.xml");
            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            factory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Error: "+ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * method to save a new product
     * @param /Models.Product product
     * @return Boolean true
     */
    public Boolean store(Product product) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(product);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to get all Products
     * @return List<Product>
     */
    public List<Product> index() {
        Session session = factory.openSession();
        Transaction tx = null;
        List products = null;
        try {
            tx = session.beginTransaction();
            products = session.createCriteria(Product.class).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return products;
    }

    /**
     * method to get a Product
     * @param /Integer id
     * @return Product product
     */
    public Product show(Integer id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Product product = null;
        try {
            tx = session.beginTransaction();
            product = (Product) session.createCriteria(Product.class)
                    .setFetchMode("category", FetchMode.JOIN)
                    .add(Restrictions.eq("id", id)).uniqueResult();
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return product;
    }

    /**
     * method to update a product
     * @param /Models.Product product
     * @return Boolean true
     */
    public Boolean update(Product product) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(product);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to delete a Product
     * @param /Model/Product/ product
     * @return Boolean true
     */
    public boolean destroy(Product product) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(product);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return true;
    }

    /**
     * method to get the number of registered products.
     * @return Integer countProducts
     */
    public long count() {
        Session session = factory.openSession();
        Transaction tx = null;
        long countProducts = 0;
        try {
            tx = session.beginTransaction();
            countProducts = (long) session.createCriteria(Product.class)
                    .setProjection(Projections.rowCount()).uniqueResult();
            tx.commit();
        } catch (HibernateException ex) {
            if (tx!=null) tx.rollback();
            ex.printStackTrace();
        } finally {
            session.close();
        }
        return countProducts;
    }
}