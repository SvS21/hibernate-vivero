package Models;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auto_gen")
    @SequenceGenerator(name = "auto_gen", sequenceName = "A")
    @Column(name = "id")
    private Integer idUser;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String last_name;
    @Column(name = "username")
    private String username;
    @Column(name = "phone")
    private BigInteger phone;
    @Column(name = "password")
    private String password;
    @Column(name = "rol")
    private Integer rol;
    @Column(name = "created_at")
    private String created_at;

    private String fullName;

    public User() {    }

    public User(String name, String last_name, String username, BigInteger phone, String password, Integer rol) {
        this.name = name;
        this.last_name = last_name;
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.rol = rol;
    }

    public Integer getIdUser() { return idUser; }

    public void setIdUser(Integer idUser) { this.idUser = idUser; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getLast_name() { return last_name; }

    public void setLast_name(String last_name) { this.last_name = last_name; }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public BigInteger getPhone() { return phone; }

    public void setPhone(BigInteger phone) { this.phone = phone; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Integer getRol() { return rol; }

    public void setRol(Integer rol) { this.rol = rol; }

    public String getCreated_at() { return created_at; }

    public void setCreated_at(String created_at) { this.created_at = created_at; }

    public String getFullName() { return name + " " + last_name; }
}