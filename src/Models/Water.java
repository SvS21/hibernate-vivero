package Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "waters")
public class Water implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auto_gen")
    @SequenceGenerator(name = "auto_gen", sequenceName = "A")
    @Column(name = "id")
    private Integer idWater;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    @Column(name = "current_date")
    private String current_date;
    @Column(name = "next_date")
    private String next_date;
    @Column(name = "created_at")
    private String created_at;

    public Water() {    }

    public Water(Product product, String current_date, String next_date) {
        this.product = product;
        this.current_date = current_date;
        this.next_date = next_date;
    }

    public Integer getIdWater() { return idWater; }

    public void setIdWater(Integer idWater) { this.idWater = idWater; }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }

    public String getCurrent_date() { return current_date; }

    public void setCurrent_date(String current_date) { this.current_date = current_date; }

    public String getNext_date() { return next_date; }

    public void setNext_date(String next_date) { this.next_date = next_date; }

    public String getCreated_at() { return created_at; }

    public void setCreated_at(String created_at) { this.created_at = created_at; }
}