package Models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "categories")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auto_gen")
    @SequenceGenerator(name = "auto_gen", sequenceName = "A")
    @Column(name = "id")
    private Integer idCategory;
    @Column(name = "name")
    private String name;
    @Column(name = "category_id")
    private Integer category_id;
    @Column(name = "created_at")
    private String created_at;
    @OneToMany
    private List<Product> products = new ArrayList<>();

    public Category() {    }

    public Category(String name) {
        this.name = name;
    }

    public Category(String name, Integer category_id) {
        this.name = name;
        this.category_id = category_id;
    }

    public Integer getIdCategory() { return idCategory; }

    public void setIdCategory(Integer idCategory) { this.idCategory = idCategory; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Integer getCategory_id() { return category_id; }

    public void setCategory_id(Integer category_id) { this.category_id = category_id; }

    public String getCreated_at() { return created_at; }

    public void setCreated_at(String created_at) { this.created_at = created_at; }

    public List<Product> getProducts() { return products; }

    public void setProducts(List<Product> products) { this.products = products; }

    @Override
    public String toString() {
        return "id =" + idCategory +", name: " + name;
    }

    @PreRemove
    public void nullificarProducts() {
        products.forEach(producto -> producto.setCategory(null));
    }
}