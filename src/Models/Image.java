package Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product_images")
public class Image implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auto_gen")
    @SequenceGenerator(name = "auto_gen", sequenceName = "A")
    @Column(name = "id")
    private Integer idImage;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    @Column(name = "created_at")
    private String created_at;

    public Image() {    }

    public Image(String name, Product product) {
        this.name = name;
        this.product = product;
    }

    public Integer getIdImage() { return idImage; }

    public void setIdImage(Integer idImage) { this.idImage = idImage; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }

    public String getCreated_at() { return created_at; }

    public void setCreated_at(String created_at) { this.created_at = created_at; }
}