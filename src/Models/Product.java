package Models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "products")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "auto_gen")
    @SequenceGenerator(name = "auto_gen", sequenceName = "A")
    @Column(name = "id")
    private Integer idProduct;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    @Column(name = "status")
    private Integer condition;
    @Column(name = "created_at")
    private String created_at;
    @OneToMany
    private List<Water> waters;
    @OneToMany
    private List<Image> images;

    public Product() {    }

    public Product(String name, Category category, Integer condition) {
        this.name = name;
        this.category = category;
        this.condition = condition;
    }

    public Integer getIdProduct() { return idProduct; }

    public void setIdProduct(Integer idProduct) { this.idProduct = idProduct; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Category getCategory() { return category; }

    public void setCategory(Category category) { this.category = category; }

    public Integer getCondition() { return condition; }

    public void setCondition(Integer condition) { this.condition = condition; }

    public String getCreated_at() { return created_at; }

    public void setCreated_at(String created_at) { this.created_at = created_at; }

    public List<Water> getWaters() { return waters; }

    public void setWaters(List<Water> waters) { this.waters = waters; }

    public List<Image> getImages() { return images; }

    public void setImages(List<Image> images) { this.images = images; }
}