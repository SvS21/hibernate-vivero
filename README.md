# Vivero
Este proyecto esta hecho en Java y Hibernate, se usa MySQL como motor de la base de datos.

### Pasos para ser un Dev Ninja 😼
- [Requerimientos](#requerimientos)
- [Descargar el proyecto](#descargar-el-proyecto)
- [Instalacion](#instalacion)
- [Entorno](#entorno)

### Requerimientos 
Necesitas esto en tú ordenador
- [Git](https://git-scm.com/downloads)
- IDE
    - [Eclipse](https://www.eclipse.org/downloads/packages/)
    - [Intellij](https://www.jetbrains.com/es-es/idea/download/)
    - [Netbeans](https://netbeans.org/downloads/8.2/rc/)
- [Java 11](https://docs.oracle.com/en/java/javase/11/install/overview-jdk-installation.html#GUID-8677A77F-231A-40F7-98B9-1FD0B48C346A)
    - [JAR's](https://gluonhq.com/products/javafx/)
- [MySQL](https://www.mysql.com/downloads/) o Gestor de servicios
    - [XAMPP](https://www.apachefriends.org/es/index.html)
    - [WampServer](https://sourceforge.net/projects/wampserver/)
- [JDBC Hibernate](https://drive.google.com/drive/folders/1ehVoEP4ID4UjNRWirfmP6Q8extKfIXRD?usp=sharing)
- [JDBC MySQL](https://drive.google.com/file/d/1EjDyXkBTrWByz1eO1RdFQXeipcLMbWgM/view?usp=sharing)

### Descargar el proyecto
- Git
    - Abre una terminal y naveja al directorio donde deseas colocar el proyecto, posteriormente ingresa los comandos.
    - Configura tus [credenciales de git](https://www.atlassian.com/es/git/tutorials/setting-up-a-repository/git-config), con los siguientes comandos:
        ```bash
        git config --global user.name "tu nombre..."
        git config --global user.email "tucorreo@...."
        git config --global user.password "tu contraseña"
        ```
    - Opciones de descarga Git
        - HTTPS
            ```bash
            git clone https://gitlab.com/SvS21/hibernate-vivero.git
            ```
        - SSH
            ```bash
            git clone git@gitlab.com:SvS21/hibernate-vivero.git
            ```
- [ZIP](https://gitlab.com/SvS21/hibernate-vivero/-/archive/master/hibernate-vivero-master.zip)

- IDE
  - Si usas este metodo puedes omitir el paso de instalación. 👍
    - [Intellij](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html#import-project)
    - [Eclipse](http://javaen.blogspot.com/2015/09/como-importar-un-proyecto-de-github-al-eclipse.html)
    - [Netbeans](https://hashblogeando.wordpress.com/2019/03/24/clonando-un-proyecto-desde-github-o-gitlab-en-netbeans/)

#### Instalacion
En una terminal, navega al directorio que contiene el proyecto, ingresa el siguiente comando
```bash
git checkout development
```
Abre tú IDE y selecciona _abrir o importar proyecto_, busca el proyecto descargado y abrelo.

Agregar los JAR's
> File/Project Structure/Libraries
- Da click en agregar JAR's de java, busca los JAR's descargados con anterioridad y agregalos al proyecto.

Seleccionar clase principal
> Run/Edit configurations
- Setea el valor de ```Main class``` por : ```Sample.Main```

Modificar ejecución
> Run/Edit configurations
- Setea el valor de ```VM options``` por : ```module-path ${PATH_TO_JAVAFX} --add-modules=javafx.controls,javafx.fxml```
- ${PATH_TO_FX} : Es una variable de entorno al instalar Java11


#### Entorno
- Inicia el servicio de MySQL
- Ejecuta este [Script SQL](https://drive.google.com/file/d/1DI58FG1GabvECWoq7ctMtZWUPdWBrANe/view?usp=sharing) en tu servicio de MySQL
- Modifica tus credenciales de conexión a MySQL para el proyecto

> Project_Dir/src/configs/hibernate-mysql.cfg.xml
```xml
<!-- URL -->
<property name="hibernate.connection.url">jdbc:mysql://localhost:{Puerto}/{Base_de_datos}</property>
<!-- User -->
<property name="hibernate.connection.username">{Nombre_de_usuario}</property>
<!-- Password -->
<property name="hibernate.connection.password">{Contraseña}</property>
```
